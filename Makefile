build:
	docker build --no-cache -t assassin-bot-discord:latest .

up:
	docker run -v ${PWD}/bot.py:/opt/bot.py --rm -it assassin-bot-discord /bin/bash

.ONESHELL:
secret:
	echo "enter discord token"
	read TOKEN
	echo "enter discord guild (probably Friendm00t)"
	read GUILD
	microk8s.kubectl create secret generic assassin-bot-discord --from-literal=discord-token=$$TOKEN --from-literal=discord-guild=$$GUILD

deploy:
	microk8s.kubectl apply -f k8s_bot.yaml
