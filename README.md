http://discordapp.com/developers/applications
add application
create bot
oauth2
        - bot from the SCOPES options
        - Administrator from BOT PERMISSIONS
        - copy oauth url, open in browser, sign in, join guild

create k8s secret:
kubectl create secret generic discord-Assassin --from-literal=discord-token='{get token from discord}' --from-literal=discord-guild='guild name (team)'
kubectl apply -f k8s_bot.yaml
