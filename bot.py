import random

from discord_base_bots.fighter import Fighter, run_bot


class Assassin(Fighter):
    bot_name = "Assassin"
    hot_word = "assassin"

    #############################
    #
    # Your round actions
    #
    #############################
    def action(self, *args):
        """What do you want to do this round?"""
        return random.choice([":crossed_swords: attack :crossed_swords:", ":shield: defend :shield:"])

    def read_reaction(self, reaction):
        """Read how your opponent reacted to *YOUR* round action."""
        if f"{self.opponent} blocks" in reaction:
            return f"{self.hot_word} staggers back"

    #############################
    #
    # Opponent round actions
    #
    #############################
    def process_opponent_action(self, action):
        """View the action your opponent took.

        Args:
            action (str): The action your opponent took this round.

        Returns:
            Optional[str]: A text response to the action.
        """
        if action.endswith("defend"):
            return "_searches your defence_"
        elif action.endswith("attack"):
            return "_attempts to counter_"

    #############################
    #
    # End conditions
    #
    #############################
    def on_victory(self, data, message):
        return ["_conjures a smoke screen_",
                f"void sacrifice {self.opponent}",
                f"_slips into the shadows_"]

    # def on_loss(self, data, message):
    #     pass

    # def on_timeout(self, data, message):
    #     pass


if __name__ == "__main__":
    from pathlib import Path
    path = Path(".env").resolve()
    run_bot(Assassin)
